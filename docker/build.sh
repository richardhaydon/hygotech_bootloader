echo 'Copy bootloader source into build system'
cp -r /ht/repo/pca10100_s140_ble/   /nrf5/nRF5_SDK_16.0.0/examples/dfu/secure_bootloader/
cp /ht/repo/main.c  /nrf5/nRF5_SDK_16.0.0/examples/dfu/secure_bootloader/
cp /ht/repo/boards/ht_board.h /nrf5/nRF5_SDK_16.0.0/components/boards/
cp /ht/repo/boards/boards.h /nrf5/nRF5_SDK_16.0.0/components/boards/boards.h

cp /ht/repo/dfu/public_key.c /nrf5/nRF5_SDK_16.0.0/examples/dfu/dfu_public_key.c

echo 'Build withing the sdk as a new project for ht'
cd /nrf5/nRF5_SDK_16.0.0/examples/dfu/secure_bootloader/pca10100_s140_ble/armgcc
make 
mergehex -m /nrf5/nRF5_SDK_16.0.0/components/softdevice/s140/hex/s140_nrf52_7.0.1_softdevice.hex  /nrf5/nRF5_SDK_16.0.0/examples/dfu/secure_bootloader/pca10100_s140_ble/armgcc/_build/nrf52833_xxaa_s140.hex -o /ht/output/t_bootloader.hex
