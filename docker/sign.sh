# Create a dfu package from the application hex file in the /dx2/output folder
# assumes output name used by the dib build docker
nrfutil pkg generate --hw-version 52 --application-version 1 --application /dx2/output/uc_dib.hex --key-file repo/dfu/private.key --sd-req 0x00 output/uc_dib.zip
#Zip created at output/uc_dib.zip
