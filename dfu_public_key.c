
/* This file was automatically generated by nrfutil on 2020-04-11 (YY-MM-DD) at 07:22:37 */

#include "stdint.h"
#include "compiler_abstraction.h"

/** @brief Public key used to verify DFU images */
__ALIGN(4) const uint8_t pk[64] =
{
    0x91, 0x07, 0xf0, 0x32, 0x26, 0x21, 0x61, 0xe0, 0xf9, 0xed, 0xdc, 0x52, 0xb1, 0x89, 0xb3, 0xcd, 0x75, 0xa9, 0x44, 0x53, 0xcd, 0x67, 0x30, 0xae, 0xfd, 0x53, 0x85, 0xf6, 0x38, 0x50, 0xf8, 0x74,
    0x9f, 0xe9, 0xc7, 0x0f, 0xec, 0xe0, 0xfc, 0x0f, 0x70, 0xda, 0x6c, 0xf8, 0x56, 0x9d, 0x54, 0xda, 0x00, 0x5e, 0x51, 0xf1, 0x83, 0xf7, 0xb8, 0x46, 0x75, 0x5a, 0x90, 0x37, 0xe5, 0x98, 0x37, 0xe3
};
